clear;

tname='2007_000033'

mat=load(strcat('data/',tname,'_slic_k600_m15.mat'));
im=mat.im;
[H, W,~]=size(im);
suppix=mat.suppix;

num_of_sup=max(max(suppix));

gt=load(strcat('data/',tname,'.mat'));
gt=gt.groundTruth{1}.Segmentation;
gt(gt==255)=0;
gt=uint8(gt);

psize=35;
im2=rgb2gray(im);


%% Get sample points
%[l,am,Sp]=get_slic(im,suppix);
%[lc,C,regionsC]=spdbscan(l,Sp,am,10);
%suppix=lc;

ucm2=load('2007_000033.mat');
c=struct2cell(ucm2);
d=c{1};
d=d(1:2:end,1:2:end);
d=d>0.05;

CC=bwlabel(1-d);
for h=1:H
    for w=1:W
        if CC(h,w)==0
            pix=im2(h,w);
            min_diff=1000;
            for hh=max(1,h-1):min(h+1:H)
                for ww=max(1,w-1):min(w+1:W)
                    if CC(hh,ww)~=0
                        pix2=im2(hh,ww);
                        if abs(pix2-pix)<min_diff
                            min_diff=abs(pix2-pix);
                            min_label=CC(hh,ww);
                        end
                    end
                end
            end
            CC(h,w)=min_label;
        end
    end
end

[I,J]=ind2sub(size(d),find(d));
sample=[I,J];
suppix=CC;
%sample=GetSamplePoints(suppix,d);

%% sample and save negative training data
half_size=int32(psize/2);
patch_id=0;
%mask=zeros(H,W);

center_thresh=5;
negOvlpThresh = 0.5;

if exist(sprintf('res/%s/neg/',tname))==0
    mkdir(sprintf('res/%s/neg/',tname));
end   

addpath('EdgeTrace');

mask=zeros(H,W);
cnt=0;
tic
for i=1:length(sample)
    h=sample(i,1)-half_size;
    w=sample(i,2)-half_size;
    if h<1 || w<1 || h+psize-1 > H || w+psize-1 > W || mask(sample(i,1),sample(i,2))==1
        continue;
    end
    
    testPatchImg = im(h:h+psize-1,w:w+psize-1,:);
    cur_sup=suppix(h:h+psize-1,w:w+psize-1);
    %cur_sup=cleanupregions(cur_sup,50,4);
     
    cur_sup_cluster = cur_sup;
    cluster_id=unique(cur_sup_cluster);
    cluster_num=length(cluster_id);
    
    cur_gt=gt(h:h+psize-1,w:w+psize-1);
    cur_gt(cur_gt~=2)=0;
    cur_gt(cur_gt==2)=1;
    
    inner_patch_id=0;
   
   res=RandomPartition(cur_sup,psize);
   res=[1];
    cnt=cnt+1;
    if mod(cnt,100)==0
        fprintf('%d %d %d %d %d\n',cnt, h,w,length(res),cluster_num);
    end
    %{
    [res_num,~]=size(res);
    if res_num<1
        continue;
    end
    rp=randi(res_num);
    for j=1:res_num
        if(j~=rp)
            continue;
        end
        str=res(j,:);
        testPatchMask=zeros(psize,psize);
        for k=1:cluster_num
           testPatchMask(cur_sup_cluster==k)=str(k);
        end
    
        ovlpGT=fbInitUni(testPatchMask,double(cur_gt));
        if ovlpGT>negOvlpThresh
            if is_center_on_boundary(testPatchMask,half_size,5)
                continue;
            end
        end
        
        inner_patch_id=inner_patch_id+1;
        if mod(inner_patch_id,4)~=1
            continue;
        end
        
        patch_res=im(h:h+psize-1, w:w+psize-1,:);
        for k=1:psize
            for l=1:psize
                if is_boundary(testPatchMask,k,l)
                    patch_res(k,l,1)=255;
                    patch_res(k,l,2)=0;
                    patch_res(k,l,3)=0;
                end
            end
        end
        
        fname=sprintf('res/%s/neg/2007_000480_%d.ppm',tname,patch_id);
        imwrite(patch_res,fname);
        patch_id=patch_id+1;
    end    
    %}
   h1=max(1,sample(i,1)-3);
   h2=min(sample(i,1)+3,H);
   w1=max(1,sample(i,2)-3);
   w2=min(sample(i,2)+3,W);
   mask(h1:h2,w1:w2)=1;
    
end
toc
   
   