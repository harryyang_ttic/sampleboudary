clear;

mat=load('data/2007_000480_slic_k600_m15.mat');
im=mat.im;
[H, W,~]=size(im);
suppix=mat.suppix;

num_of_sup=max(max(suppix));

gt=load('data/2007_000480.mat');
gt=gt.groundTruth{1}.Segmentation;
gt(gt==255)=0;

psize=39;
im2=rgb2gray(im);

%% Get sample points
mask=zeros(H,W);
i=1;
for h=1:H
    for w=1:W
        if is_boundary(suppix,h,w) && mask(h,w)==0
           sample(i,:)=[h,w];
           h1=max(1,h-5);
           h2=min(h+5,H);
           w1=max(1,w-5);
           w2=min(w+5,W);
           mask(h1:h2,w1:w2)=1;
           i=i+1;  
        end  
    end
end

gt=load('data/2007_000480.mat');
gt=gt.groundTruth{1}.Segmentation;
gt(gt==255)=0;
gt=uint8(gt);

%% sample and save positive training data
patch_id=0;
for i=1:length(sample)
    h=sample(i,1);
    w=sample(i,2);
    if h+psize-1 > H || w+psize-1 > W
        continue;
    end
    cur_patch=im2(h:h+psize-1,w:w+psize-1);
    cur_sup=suppix(h:h+psize-1,w:w+psize-1);
    cur_gt=gt(h:h+psize-1,w:w+psize-1);
    cur_gt(cur_gt~=2)=0;
    cur_gt(cur_gt==2)=1;
    if nnz(cur_gt)==0 || nnz(cur_gt)==psize*psize
        continue;
    end
    %sup_cluster=segment_patch(cur_patch,cur_sup); 
    sup_cluster=cur_sup;
    sup_cluster_u=unique(sup_cluster);
    tmp_patch=zeros(psize,psize);
    flag=false;
    for j=1:length(sup_cluster_u)
        sum_gt=sum(sum(cur_gt(sup_cluster==sup_cluster_u(j))));
        sum_fg=nnz(sup_cluster==sup_cluster_u(j));
        if sum_gt>sum_fg*0.85
            tmp_patch(sup_cluster==sup_cluster_u(j))=1;
        elseif sum_gt>sum_fg*0.15
            flag=true;
        end
    end
    if flag==true
        continue;
    end
    sum_fg=nnz(tmp_patch);
    if sum_fg>0.95*psize*psize || sum_fg<0.05*psize*psize
        continue;
    end
    patch_res=im(h:h+psize-1, w:w+psize-1,:);
    for k=1:psize
        for l=1:psize
            if is_boundary(tmp_patch,k,l)
                patch_res(k,l,1)=255;
                patch_res(k,l,2)=0;
                patch_res(k,l,3)=0;
            end
        end
    end
    fname=sprintf('res/2007_000480_%d.ppm',patch_id);
    imwrite(patch_res,fname);
    patch_id=patch_id+1;
end


