function [root] = bsdsRoot()
% function [root] = bsdsRoot()
%
% Return the root directory for the BSDS data.
%
% David R. Martin <dmartin@eecs.berkeley.edu>
% March 2003


%if ispc
  root = '/share/data/vision-greg/BSD/BSDS300/';
%end
