mat=load('data/2007_000033_slic_k600_m15.mat');
im=mat.im;
suppix=mat.suppix;

[H W,~]=size(im);
res=im;
for h=1:H
    for w=1:W
        if is_boundary(suppix,h,w)
            res(h,w,1)=255;
            res(h,w,2)=0;
            res(h,w,3)=0;
        end  
    end
end

imshow(res);
imwrite(res,'vis/slic.ppm');
