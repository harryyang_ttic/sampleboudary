function [ res ] = segment_patch(patch, sup)
%SEGMENT_PATCH Summary of this function goes here
%   Detailed explanation goes here
id=unique(sup);
colors=zeros(length(id),1);

for i=1:length(id)
    color=mean2(patch(sup==id(i)));
    colors(i)=color;
end

cluster_id=clusterdata(colors,'criterion','distance','cutoff',30);

res=zeros(size(sup));
for i=1:length(id)
    res(sup==id(i))=cluster_id(i);  
end

end
