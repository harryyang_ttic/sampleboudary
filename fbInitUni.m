function [ ovlp ] = fbInitUni( img1,img2 )
%FBINITUNI Summary of this function goes here
%   Detailed explanation goes here
inter = img1.*img2;
uni = double(img1+img2);
fgivu = length(find(inter))/length(find(img1+img2));
bgivu = length(find(~uni)) / length(find(~inter));
ovlp = ( fgivu + bgivu ) /2;

end

