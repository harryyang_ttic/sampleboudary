mat=load('data/2007_000480_slic_k600_m15.mat');
im=mat.im;
[H, W,~]=size(im);
suppix=mat.suppix;

num_of_sup=max(max(suppix));

gt=load('data/2007_000480.mat');
gt=gt.groundTruth{1}.Segmentation;
gt(gt==255)=0;

% make sure it's odd
psize=59;

patch_id=1;
for i=1:num_of_sup
    %% check if it's on boundary
    reg=suppix;
    reg(reg~=i)=0;
    reg(reg==i)=1;
    tmp1=reg;
    sum1=sum(sum(tmp1));
    tmp1(gt~=2)=0;
    sum2=sum(sum(tmp1));
    if(sum1==sum2)
        continue;
    end
    tmp1(gt==2)=0;
    sum3=sum(sum(tmp1));
    if(sum2==sum3)
        continue;
    end
    
    %% find the center
    s=regionprops(reg,'centroid');
    s=s.Centroid;
    s=int32(s);
    fprintf('%d %d %d\n',patch_id, s(1),s(2));
    patch=zeros(psize,psize,3);
    offset=(psize-1)/2;
    x0=s(1);
    y0=s(2);
    for j=y0-offset:y0+offset
        for k=x0-offset:x0+offset
            if j<1 || j>H || k<1 || k>W
               continue;
            end
            y=j-y0+offset+1;
            x=k-x0+offset+1;
            patch(y,x,:)=im(j,k,:);
        end
    end
    
    fname=sprintf('res/2007_000480_%d.ppm',patch_id);
    patch=uint8(patch);
    imwrite(patch,fname);
    patch_id=patch_id+1;
end
