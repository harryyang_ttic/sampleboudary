function [ res ] = Partition( cur_sup,psize )
%PARTITION Summary of this function goes here
%   Detailed explanation goes here

%% re-label
res=[];

unique_id=unique(cur_sup);
cluster_num=length(unique_id);
cur_pat=zeros(size(cur_sup));
for i=1:length(unique_id)
    cur_pat(cur_sup==unique_id(i))=i;
end
unique_id=unique(cur_pat);
%% find center

[H W]=size(cur_pat);
half_size=int32(psize/2);
center_patch=cur_pat(half_size-3:half_size+3,half_size-3:half_size+3);
center_id=unique(center_patch);
if length(center_id)==1
    return;
end

%% find interior
is_inside=zeros(cluster_num,1);
for i=1:cluster_num
    [I, J]=ind2sub(size(cur_pat),find(cur_pat==unique_id(i)));
    u1=min([I J],[],1);
    u2=max([I J],[],1);
    if u1(1)>1 && u1(2)>1 && u2(1)<H && u2(2)<W
        is_inside(i)=1;
    end
end

%% find adjacency matrix
adj=zeros(length(unique_id),length(unique_id));
for h=1:H
    for w=1:W
        label=cur_pat(h,w);
        if h-1>0 && cur_pat(h-1,w)~=label
            adj(label,cur_pat(h-1,w))=adj(label,cur_pat(h-1,w))+1;
        end
         if h+1<H && cur_pat(h+1,w)~=label
            adj(label,cur_pat(h+1,w))=adj(label,cur_pat(h+1,w))+1;
         end
         if w-1>0 && cur_pat(h,w-1)~=label
            adj(label,cur_pat(h,w-1))=adj(label,cur_pat(h,w-1))+1;
         end
         if w+1<W && cur_pat(h,w+1)~=label
            adj(label,cur_pat(h,w+1))=adj(label,cur_pat(h,w+1))+1;  
        end
    end
end
adj=adj>0;
adj=double(adj);
padj=GraphPartition(adj);

count=1;

for j=1:length(padj)
    mask=zeros(cluster_num,1);
    m=cell2mat(padj(j));
    mask(m)=1;
   
    A=ismember(unique_id,center_id);
    if length(unique(mask(A)))==1
        continue;
    end
    
    if length(unique(mask(~is_inside)))==1
       continue;
    end
    
    B=adj(~mask,~mask);
    n=networkComponents(B);
    if n>1 
        continue;
    end
    
   % res2=zeros(size(cur_pat));
    
  %  for i=1:cluster_num
  %      if mask(i)==1
 %           res2(cur_pat==i)=1;
 %       end
 %   end
  %  imshow(res2);
    res(count,:)=mask;
    res(count+1,:)=~mask;
    count=count+2;
    
end

end

