function [ res ] = GraphPartition( adj )
%GRAPHPARTITION Summary of this function goes here
%   Detailed explanation goes here
[H,~]=size(adj);

res=cell(0);
res(1)={1};
start=1;
for i=2:H
    start2=length(res);
    for j=start:start2
        P=cell2mat(res(j));
        P2=adj(P,:);
        P3=sum(P2,1);
        fa=find(P3);
        tt=max(P);
        for k=1:length(fa)
            if fa(k)<=tt
                continue;
            end
            t=union(P,fa(k));
            if length(t)==i
                res(end+1)={t};
            end
        end
    end
    start=start2+1;
    if length(res)>4
        return;
    end
end

end

