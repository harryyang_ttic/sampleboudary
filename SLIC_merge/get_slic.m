function [ l, Am, Sp ] = get_slic( im,SLIC )
%GET_SLIC Summary of this function goes here
%   Detailed explanation goes here
im=double(im);
[rows, cols, ~] = size(im);
[l, Am] = mcleanupregions(SLIC, 1);

N = length(Am);
Sp = struct('L', cell(1,N), 'a', cell(1,N), 'b', cell(1,N), ...
                'stdL', cell(1,N), 'stda', cell(1,N), 'stdb', cell(1,N), ...
                'r', cell(1,N), 'c', cell(1,N), 'N', cell(1,N));
[X,Y] = meshgrid(1:cols, 1:rows);
L = im(:,:,1);    
A = im(:,:,2);    
B = im(:,:,3);    
for n = 1:N
    mask = l==n;
    nm = sum(mask(:));

    Sp(n).L = sum(L(mask))/nm;
    Sp(n).a = sum(A(mask))/nm;
    Sp(n).b = sum(B(mask))/nm;

    Sp(n).r = sum(Y(mask))/nm;
    Sp(n).c = sum(X(mask))/nm;

    % Compute standard deviations of the colour components of each super
    % pixel. This can be used by code seeking to merge superpixels into
    % image segments.  Note these are calculated relative to the mean colour
    % component irrespective of the centre being calculated from the mean or
    % median colour component values.
    Sp(n).stdL = std(L(mask));
    Sp(n).stda = std(A(mask));
    Sp(n).stdb = std(B(mask));

    Sp(n).N = nm;  % Record number of pixels in superpixel too.
end

end

