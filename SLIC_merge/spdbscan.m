function [lc, C, regionsC] = spdbscan(l, Sp, Am, Ec)
    
    minPts = 1;  
    Np = length(Sp);
    
    regionsC  = zeros(Np,1);
    C     = {};
    Nc    = 0;               % Cluster counter.
    Pvisit = zeros(Np,1);    % Array to keep track of superpixels that have
                             % been visited. 
    for n = 1:Np

       if ~Pvisit(n)                  % If this superpixel not visited yet...
           Pvisit(n) = 1;                          % mark it as visited
           neighbours = regionQueryM(Sp, Am, n, Ec); % and find its neighbours

           % Form a cluster...
           Nc = Nc + 1;      % Increment number of clusters and process
                             % neighbourhood.
                           
           C{Nc} = [n];      % Initialise cluster Nc with point n
           regionsC(n) = Nc; % and mark superpixel n as being a member of cluster Nc.
               
           ind = 1;          % Initialise index into neighbourPts array.
               
           % For each superpixel Sp' in list of neighbours ...
           while ind <= length(neighbours)
               
               nb = neighbours(ind);
               
               if ~Pvisit(nb)        % If this neighbour has not been visited
                   Pvisit(nb) = 1;   % mark it as visited.
                   
                   % Find the neighbours of this neighbour and 
                   % add them to the neighbours list
                   neighboursP = regionQueryM(Sp, Am, nb, Ec);
                   neighbours = [neighbours  neighboursP];
               end            
               
               % If this neighbour nb not yet a member of any cluster add it
               % to this cluster.
               if ~regionsC(nb)  
                   C{Nc} = [C{Nc} nb];
                   regionsC(nb) = Nc;
               end
               
               ind = ind + 1;  % Increment neighbour point index and process
                               % next neighbour
           end

       end
    end
    
    % Generate new labeled image corresponding to the new clustered regions
    lc = zeros(size(l));
    for n = 1:length(regionsC)
        lc(l==n) = regionsC(n);
    end

        
    
%------------------------------------------------------------------------
% Find indices of all superpixels adjacent to superpixel n with mean colour
% difference less than Ec.  Use CMC colour difference measure
%
% Arguments:
%             Sp - The struct array of superpixel attributes
%             An - Adjacency matrix
%              n - Index of superpixel being considered
%             Ec - Colour distance threshold

function neighbours = regionQueryCMC(Sp, Am, n, Ec)
    
    lw = 1;
    neighbours = [];
    
    % Get indices of all superpixels connected to superpixel n
    ind = find(Am(n,:));
    
    for i = ind
        
        dE = cmcdifference([Sp(i).L; Sp(i).a; Sp(i).b],...
                           [Sp(n).L; Sp(n).a; Sp(n).b], lw);
        
        if dE < Ec
            neighbours = [neighbours i];     
        end
    end

    
%------------------------------------------------------------------------
% Find indices of all superpixels adjacent to superpixel n with mean colour
% difference less than Ec.
%
% Arguments:
%             Sp - The struct array of superpixel attributes
%             An - Adjacency matrix
%              n - Index of point of interest
%             Ec - Colour distance threshold

function neighbours = regionQueryM(Sp, Am, n, Ec)
    
    E2 = Ec^2;   
    neighbours = [];
    
    % Get indices of all superpixels connected to superpixel n
    ind = find(Am(n,:));
    
    for i = ind
        % Test if distance^2 < E^2 
        v = [Sp(i).L; Sp(i).a; Sp(i).b] - ...
            [Sp(n).L; Sp(n).a; Sp(n).b];

        dist2 = v'*v;
        if dist2 < E2 
            neighbours = [neighbours i];     
        end
    end
    