for h=1:H
    for w=1:W
        if CC(h,w)==0
            pix=im2(h,w);
            min_diff=1000;
            for hh=max(1,h-1):min(h+1:H)
                for ww=max(1,w-1):min(w+1:W)
                    if CC(hh,ww)~=0
                        pix2=im2(hh,ww);
                        if abs(pix2-pix)<min_diff
                            min_diff=abs(pix2-pix);
                            min_label=CC(hh,ww);
                        end
                    end
                end
            end
            CC(h,w)=min_label;
        end
    end
end
