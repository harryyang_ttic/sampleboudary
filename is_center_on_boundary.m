function [flag] = is_center_on_boundary(patch,half_size, thresh)
    flag=false;
    for k=half_size-thresh:half_size+thresh
        for l=half_size-thresh:half_size+thresh
            if is_boundary(patch,k,l)
                flag=true;
            end
        end
    end
end
